<?php

use Passendo\AdServer;

require_once('./vendor/autoload.php');

$adServer = new AdServer();

$publisherKeyValues = ['age' => 35, 'category' => 'programming'];
$advertiserConditions = '(age=35 and category=programming) or (age=25 and category=economics)';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result); // true

$publisherKeyValues = ['age' => 35, 'category' => 'economics'];
$advertiserConditions = '(age=35 and category=programming) or (age=25 and category=economics)';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result); // false

$publisherKeyValues = ['age' => 35, 'category' => 'programming'];
$advertiserConditions = '(age>=18 and age <=30) or (age>40 and age<60)';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result);  // false

// range
$publisherKeyValues = ['age' => 32, 'category' => 'programming'];
$advertiserConditions = '(age=[30-35] and category=programming) or (age=[25-30] and category=economics)';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result);  // true

// comma separated values
$publisherKeyValues = ['zip' => 11010, 'category' => 'programming'];
$advertiserConditions = '(zip=11000,11010,11009 and category=programming) or (zip=11005,11011,11015 and category=economics)';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result);  // true

// free form
$publisherKeyValues = ['age' => 35, 'category' => 'economics', 'foo' => 'bar'];
$advertiserConditions = '(age>35 and (age=25 or category=programming)) or foo=bar and age<=35';
$result = $adServer->shouldAdBeServed($publisherKeyValues, $advertiserConditions);
var_dump($result);  // true
