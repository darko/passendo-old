<?php

namespace Passendo\Parser;

class Parser
{
    public function parse(string $expression, array $variables = [])
    {
        $tokens  = (new Tokenizer())->tokenize($expression)->buildPostfixNotation();

        $stack = [];

        foreach ($tokens as $token) {
            if ($token->type == Token::T_LITERAL) {
                $stack[] = $token;
            } elseif ($token->type == Token::T_VARIABLE) {
                if (array_key_exists($token->value, $variables)) {
                    $value = $variables[$token->value];
                } else {
                    $value = false;
                    // throw new \Exception(sprintf('Unknown variable "%s"', $token->value));
                }
                $stack[] = new Token(Token::T_LITERAL, $value, $token->cursor);
            } elseif ($token->type == Token::T_OPERATOR) {
                $right = array_pop($stack);
                $left = array_pop($stack);
            
                $stack[] = (new Operation($token->value, $left, $right))->evaluate();
            }
        }
        $result = array_pop($stack);

        return $result->value;
    }   
}
