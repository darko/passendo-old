<?php

namespace Passendo\Parser;

class Operation
{
    protected $operator;
    protected $left;
    protected $right;
    
    public function __construct(string $operator, Token $left, Token $right)
    {
        $this->operator = $operator;
        $this->left = $left;
        $this->right = $right;
    }

    public function evaluate()
    {
        $operator = $this->operator;
        $left = $this->left->value;
        $right = $this->right->value;

        if ((!is_numeric($right)) and  !in_array($operator, ['and', 'or', '='])) {
            throw new \Exception(sprintf('Invalid usage of "%s" operator at position %d.', $operator, $this->right->cursor - strlen($operator)));
        }
        
        switch ($operator) {
            case 'or':
                $result =  $left || $right;
                break;
            case 'and':
                $result =  $left && $right;
                break;
            case '>':
                $result =  $left > $right;
                break;
            case '<':
                $result =  $left < $right;
                break;
            case '>=':
                $result =  $left >= $right;
                break;
            case '<=':
                $result =  $left <= $right;
                break;
            case '=':
                if (preg_match('/\[([0-9]+)-([0-9]+)\]/', $right, $match)) {
                    $result = ($left >= $match[1] and $left <= $match[2]);
                    
                } elseif (preg_match('/[a-z0-9]+(?:,[a-z0-9]+)+/', $right)) {
                    $result = in_array($left, explode(',', $right));
                } else {
                    $result =  $left == $right;
                }
                break;
        }

        return new Token(Token::T_LITERAL, $result);
    }
}
