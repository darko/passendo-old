<?php

namespace Passendo\Parser;

use Passendo\Parser\Token;
use Exception;

class Tokenizer
{
    public $tokens;
    
    private $operators = [
        'or' => 1,
        'and' => 2,
        '=' => 3,
        '<' => 3,
        '>' => 3,
        '>=' => 3,
        '<=' => 3
    ];

    public function tokenize(string $input)
    {
        $input = str_replace(["\r", "\n", "\t", "\v", "\f"], ' ', $input);
        $cursor = 0;
        $end = strlen($input);
        $parentheses = [];
        $tokens = [];

        while ($cursor < $end) {
            if ($input[$cursor] == ' ') {
                ++$cursor;
                continue;
            }
            
            if (preg_match('/[a-zA-Z0-9_-]+(?=\s*[<>=])/A', $input, $match, 0, $cursor)) {
                // variables
                $this->tokens[] = new Token(Token::T_VARIABLE, $match[0], $cursor);
                $cursor += strlen($match[0]);
            } elseif (preg_match('/(?<=[<>=])[a-zA-Z0-9\.\]\[\,_-]+/A', $input, $match, 0, $cursor)) {
                // literals
                $this->tokens[] = new Token(Token::T_LITERAL, $match[0], $cursor);
                $cursor += strlen($match[0]);
            } elseif ($input[$cursor] == '(') {
                // left parenthesis
                $parentheses[] = $cursor;

                $this->tokens[] = new Token(Token::T_LEFT_PARENTHESIS, $input[$cursor], $cursor);
                ++$cursor;
            } elseif ($input[$cursor] == ')') {
                // right parenthesis
                if (empty($parentheses)) {
                    throw new Exception(sprintf('Unexpected "%s" at position %d.', $input[$cursor], $cursor));
                }
                array_pop($parentheses);

                $this->tokens[] = new Token(Token::T_RIGHT_PARENTHESIS, $input[$cursor], $cursor);
                ++$cursor;
            } elseif (preg_match('/[<>=]{1,2}|(?<=^|[\s)])or(?=[\s(])|(?<=^|[\s)])and(?=[\s(])/A', $input, $match, 0, $cursor)) {
                // operators
                $this->tokens[] = new Token(Token::T_OPERATOR, $match[0], $cursor);
                $cursor += strlen($match[0]);
            } else {
                throw new \Exception(sprintf('Unexpected character "%s" at position %d.', $input[$cursor], $cursor));
            }
        }

        if (!empty($parentheses)) {
            $position = array_pop($parentheses);
            throw new \Exception(sprintf('Unclosed "(" at position %d.', $position));
        }

        return $this;
    }


    public function buildPostfixNotation()
    {
        $queue = [];
        $stack = [];

        foreach ($this->tokens as $token) {
            switch ($token->type) {
                case Token::T_VARIABLE:
                case Token::T_LITERAL:
                    $queue[] = $token;
                    break;
                case Token::T_LEFT_PARENTHESIS:
                    $stack[] = $token;
                    break;
                case Token::T_OPERATOR:
                    if (!array_key_exists($token->value, $this->operators)) {
                        throw new Exception(sprintf('Unknown operator "%s" at position %d.', $stack[count($stack) - 1]->value, $stack[count($stack) - 1]->cursor));
                    }

                    $op1 = $this->operators[$token->value];
                    while (count($stack) > 0 and $stack[count($stack) - 1]->type === Token::T_OPERATOR) {
                        if (!array_key_exists($stack[count($stack) - 1]->value, $this->operators)) {
                            throw new Exception(sprintf('Unknown operator "%s" at position %d.', $stack[count($stack) - 1]->value, $stack[count($stack) - 1]->cursor));
                        }
                        $op2 = $this->operators[$stack[count($stack) - 1]->value];
                        if ($op2 >= $op1) {
                            $queue[] = array_pop($stack);
                            continue;
                        }
                        break;
                    }
                    $stack[] = $token;
                    break;
                case Token::T_RIGHT_PARENTHESIS:
                    while (true) {
                        $ctoken = array_pop($stack);
                        if ($ctoken->type === Token::T_LEFT_PARENTHESIS) {
                            break;
                        }
                        $queue[] = $ctoken;
                    }
                    break;
            }
        }
        while (count($stack) !== 0) {
            $queue[] = array_pop($stack);
        }

        return $queue;
    }
}
