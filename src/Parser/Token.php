<?php

namespace Passendo\Parser;

class Token
{
    public const T_OPERATOR = 'operator';
    public const T_LITERAL = 'literal';
    public const T_VARIABLE = 'variable';
    public const T_LEFT_PARENTHESIS = 'left parenthesis';
    public const T_RIGHT_PARENTHESIS = 'right parenthesis';

    public $value;
    public $type;
    public $cursor;

    public function __construct(string $type, $value, int $cursor = null)
    {
        $this->type = $type;
        $this->value = $value;
        $this->cursor = $cursor;
    }
}
