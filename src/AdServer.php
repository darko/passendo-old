<?php

namespace Passendo;

use Passendo\Parser\Parser;
use Throwable;

class AdServer
{
    public function shouldAdBeServed(array $publisherKeyValues, string $advertiserConditions)
    {
        $parser = new Parser();
        try {
            return $parser->parse($advertiserConditions, $publisherKeyValues);
        } catch (Throwable $e) {
            return $e->getMessage();
        }
    }
}
